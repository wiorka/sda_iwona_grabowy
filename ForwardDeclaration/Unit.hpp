/*
 * Unit.hpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include <string>

#ifndef GROUP_HPP_
#define GROUP_HPP_

class Group;
class Unit
    {
    std::string mID;
    Group* mGroupPrt;
public:
    Unit (std::string ID);
    void addToGroup(Group* group);
    void printID();
    void replicate();
    };

#endif /* UNIT_HPP_ */



