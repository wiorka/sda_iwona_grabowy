/*
 * Unit.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include <string>
#include <iostream>
#include <cstdlib>
#include "Unit.hpp"
#include "Group.hpp"

Unit::Unit(std::string ID) :
	mID(ID), mGroupPrt(0)
    {
    }

void Unit::addToGroup(Group* group)
    {
    mGroupPrt = group;
    }

void Unit::printID()
    {
    std::cout << mID << std::endl;
    }

void Unit::replicate()
    {
    int idVariation = std::rand() % 10;
    char newID = '0' + idVariation;
    Unit * newborn = new Unit(mID + newID);
    newborn->addToGroup(mGroupPrt);
    mGroupPrt->add(newborn);
    }

