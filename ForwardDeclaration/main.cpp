/*
 * main.cpp
 *
 *  Created on: 27.04.2017
 *      Author: RENT
 */
#include <ctime>
#include <cstdlib>
#include "Unit.hpp"
#include "Group.hpp"

int main()
    {
    srand(time(NULL));

    Group armyOne;
    Unit* motherOne = new Unit ("1");

    armyOne.add(motherOne);
    motherOne->addToGroup(&armyOne);

    Unit*  motherTwo = new Unit ("2");
    armyOne.add(motherTwo);
    motherTwo->addToGroup(&armyOne);

    armyOne.printUnits();
    armyOne.replicateGroup();
    armyOne.printUnits();
    armyOne.replicateGroup();
    armyOne.printUnits();
    armyOne.replicateGroup();

    return 0;
    }

