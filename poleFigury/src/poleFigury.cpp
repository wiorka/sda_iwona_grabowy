//============================================================================
// Name        : poleFigury.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
class Figura
{
public:
	virtual float pole() const = 0;
	virtual ~Figura()
	{
	}
};

class Prostokat: public Figura
{
public:
	Prostokat(const float a, const float b) :
			mA(a), mB(b)
	{
	}
	~Prostokat()
	{
	}

	float pole() const
	{
		return mA * mB;
	}
private:
	float mA;
	float mB;
};

class Kolo: public Figura
{
public:
	Kolo(const float r) :
			mR(r)
	{
	}
	~Kolo()
	{
	}
	float pole() const
	{
		return pi * mR * mR;
	}
private:
	float mR;
	const float pi = 3.14159;
};

class Trojkat: public Figura
{
public:
	Trojkat(const float a, const float h) :
			mA(a), mH(h)
	{
	}
	~Trojkat()
	{
	}
	float pole() const
	{
		return 0.5 * mA * mH;
	}
private:
	float mA;
	float mH;
};

class Kwadrat: public Figura
{
public:
	Kwadrat(const float a) :
			mA(a)
	{
	}
	~Kwadrat()
	{
	}
	float pole() const
	{
		return mA * mA;
	}

private:
	float mA;
};

void wyswietlPole(Figura &figura)
{
	std::cout << figura.pole() << std::endl;
}

int main()
{
	Prostokat jakisProstokat(5, 10);
	Kolo jakiesKolo(3);
	Trojkat jakisTrojkat(4, 7.9);
	Kwadrat jakisKwadrat(8);

	Figura *wskJakasFigura = 0;

	std::cout << jakisProstokat.pole() << std::endl;
	std::cout << jakiesKolo.pole() << std::endl;
	std::cout << jakisTrojkat.pole() << std::endl;
	std::cout << jakisKwadrat.pole() << std::endl;

	wskJakasFigura = &jakisProstokat;
	std::cout << wskJakasFigura->pole() << std::endl;
	wskJakasFigura = &jakiesKolo;
	std::cout << wskJakasFigura->pole() << std::endl;
	wskJakasFigura = &jakisTrojkat;
	std::cout << wskJakasFigura->pole() << std::endl;
	wskJakasFigura = &jakisKwadrat;
	std::cout << wskJakasFigura->pole() << std::endl;

	wyswietlPole(jakisProstokat);
	wyswietlPole(jakiesKolo);
	wyswietlPole(jakisTrojkat);
	wyswietlPole(jakisKwadrat);
	return 0;
}
