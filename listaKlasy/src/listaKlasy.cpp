//============================================================================
// Name        : listaKlasy.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#ifndef LISTA_HPP_
#define LISTA_HPP_

#include <iostream>

class Lista
{
private:
	class Wezel
	{
	public:
		int mWartosc;
		Wezel* nast;

		Wezel (int wartosc)
		{
			mWartosc = wartosc;
			nast = 0;
		}
	};

	Wezel* head;
	Wezel* tail;

public:
	Lista()
	{
		head = 0;
		tail = 0;
	}

	void wypisz ()
	{
		Wezel* tmp = 0;
		tmp = head;

		std::cout << "{ ";
		while (tmp != 0)
		{
			std::cout << "[" << tmp->mWartosc << "] ";
			tmp = tmp->nast;
		}
		std::cout << " }" << std::endl;
	}


	void dodajPoczatek(int wartosc)
	{
		if (head != 0)
		{
			Wezel* nowyWezel = new Wezel(wartosc);
			nowyWezel->nast = head;
			head = nowyWezel;
		}
		else
		{
			head = new Wezel(wartosc);
			tail = head;
		}
	}

	void dodajKoniec(int wartosc)
	{
		if (head != 0)
		{
			Wezel* nowyWezel = new Wezel(wartosc);
			tail->nast = nowyWezel;
			tail = nowyWezel;

		}
		else
		{
			head = new Wezel(wartosc);
			tail = head;
		}
	}

	int pobierz (int indeks)
	{
		Wezel* tmp = head;

		int licznikOdwiedzin = 0;

		while(tmp!=0)
		{
			licznikOdwiedzin++;
			if(licznikOdwiedzin==indeks)
			{
				std::cout << tmp->mWartosc << std::endl;
				return tmp->mWartosc;
			}
			else
			{
				tmp=tmp->nast;
			}
		}

		return 0;
	}


	int znajdzPozycje(int wartosc)
	{
		Wezel* tmp = head;
		int pozycja = 0;
		while(tmp!=0)
		{
			pozycja++;
			if(tmp->mWartosc==wartosc)
			{
				std::cout << pozycja << std::endl;
				return pozycja;
			}
			else
			{
				tmp=tmp->nast;
			}
		}

		return pozycja;
	}


	bool czyPusta()
	{
		if (head == 0)
			return true;
		else
		return false;
	}

	void wyczysc()
	{
		Wezel* tmp = 0;
		tmp = head;

		while (tmp != 0)
		{
			head = tmp->nast;
			delete tmp;
			tmp = head;
		}

		head = 0;
		tail = 0;
	}


	void usun (int indeks)
	{
		Wezel* tmp = head;
		Wezel* pop = 0;
		int licznikOdwiedzin = 0;

		while(tmp!=0)
		{
			licznikOdwiedzin++;
			if(licznikOdwiedzin==indeks)
			{
				pop->nast = tmp->nast;
				delete tmp;
			}
			else
			{
				pop = tmp;
				tmp=tmp->nast;
			}
		}

	}

};

int main()
{
	Lista lista;
	lista.dodajPoczatek(15);
	lista.dodajPoczatek(10);
	lista.dodajKoniec(2);
	lista.wypisz();
	lista.pobierz(2);
	lista.znajdzPozycje(15);




	return 0;
}

#endif /* LISTA_HPP_ */
