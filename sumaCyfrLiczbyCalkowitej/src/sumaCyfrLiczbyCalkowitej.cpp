//============================================================================
// Name        : sumaCyfrLiczbyCalkowitej.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main()
{
	cout << "Podaj liczb�, kt�rej cyfry chcesz zsumowac: " << endl;
	int liczba;
	cin >> liczba;
	int suma = 0;
	while (liczba != 0)
	{
		suma += (liczba % 10);
		liczba = liczba / 10;
	}
	if (suma < 0)
	{
		suma = -suma;
	}
	cout << "Suma cyfr wynosi: " << suma << endl;

	return 0;
}
