/*
 * main.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#include <iostream>
using namespace std;
#include "Wektor.hpp"

int main()
    {

    Wektor w1(Punkt(3, 4), Punkt(6, 7));
    Wektor w2(Punkt(4, 6), Punkt(2, 8));

    w1.wypisz();
    w2.wypisz();

    Punkt pkt(5, 6);
    Wektor w3(pkt * 2, 5 * pkt);
    w3.wypisz();
    !w1;
    w1.wypisz();

    Wektor w4 = w2;
    if (w4 == w2)
	{
	std::cout << "rowne" << std::endl;
	}
    else
	{
	std::cout << "rozne" << std::endl;
	}

    return 0;
    }
