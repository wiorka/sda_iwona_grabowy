//============================================================================
// Name        : samochod.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <string>
#include <iostream>

using namespace std;

class Samochod
{
public:
	virtual void podajKolor()=0;
	virtual void podajMarke()=0;
	virtual void podajPojemnosc()=0;

	virtual ~Samochod()
	{
		cout<<"~Samochod\n";
	}

	Samochod(string kolor, string marka, float pojemnosc)
	: mKolor(kolor)
	, mMarka(marka)
	, mPojemnosc(pojemnosc)
	{
		cout<<"Samochod\n";
	}

protected:
string mKolor;
string mMarka;
float mPojemnosc;

};

class Lada : public Samochod
{
public:
	Lada(string kolor, string marka, float pojemnosc)
	: Samochod(kolor, marka, pojemnosc)
	{
		cout<<"Lada\n";
	}

	~Lada()
	{
		cout<<"~Lada\n";
	}

	void podajKolor()
	{
		cout<<"Lada kolor="<<mKolor<<endl;
	}

	void podajMarke()
	{
		cout<<"Lada marka="<<mMarka<<endl;
	}

	void podajPojemnosc()
	{
		cout<<"Lada pojemnosc="<<mPojemnosc<<endl;
	}

};

class Nissan : public Samochod
{
public:
	Nissan(string kolor, string marka, float pojemnosc)
	: Samochod(kolor, marka, pojemnosc)
	{
		cout<<"Nissan\n";
	}

	~Nissan()
	{
		cout<<"~Nissan\n";
	}

	void podajKolor()
	{
		cout<<"Nissan kolor="<<mKolor<<endl;
	}

	void podajMarke()
	{
		cout<<"Nissan marka="<<mMarka<<endl;
	}

	void podajPojemnosc()
	{
		cout<<"Nissan pojemnosc="<<mPojemnosc<<endl;
	}

};

int main()
{
	Lada niva("zielony","niva", 1.6);

	niva.podajKolor();
	niva.podajMarke();
	niva.podajPojemnosc();


	Samochod* tab[3];
	tab[0] = new Lada("bia�y","Samara",1.9);
	tab[1] = new Nissan("czarny", "Micra", 1.3);
	tab[2] = new Nissan("zielony", "Patrol", 4.0);

	for(int i=0; i<3; i++ )
	{
		tab[i]->podajKolor();
		tab[i]->podajMarke();
		tab[i]->podajPojemnosc();
	}

	for(int i=0; i<3; i++ )
	{
	delete tab[i];
	}

}
