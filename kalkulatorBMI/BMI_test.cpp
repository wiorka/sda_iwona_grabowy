///*
// * BMI_test.cpp
// *
// *  Created on: 22.04.2017
// *      Author: RENT
// */
//#include "gtest/gtest.h"
//#include "BMI.hpp"
//
//TEST(BMItest, DefaultConstruction) {
//    BMI cd;
//    EXPECT_EQ(0, cd.getWaga());
//    EXPECT_EQ(0, cd.getWzrost());
//    EXPECT_EQ(0, cd.getWiek());
//    EXPECT_EQ(nieokreslona, cd.getPlec());
//}
//
//TEST(BMItest, Construction) {
//    BMI c (80, 1.76, 35, mezczyzna);
//    EXPECT_NEAR(80, c.getWaga(), 0.1);
//    EXPECT_NEAR(1.76, c.getWzrost(), 0.1);
//    EXPECT_EQ(35, c.getWiek());
//    EXPECT_EQ(mezczyzna, c.getPlec());
//}
//
//TEST(BMItest, GetterSetter) {
//    BMI cd;
//    EXPECT_EQ(0, cd.getWaga());
//    EXPECT_EQ(0, cd.getWzrost());
//    EXPECT_EQ(0, cd.getWiek());
//    EXPECT_EQ(nieokreslona, cd.getPlec());
//
//    cd.setWaga(100);
//    cd.setWzrost(1.56);
//    cd.setWiek(80);
//    cd.setPlec(kobieta);
//
//    EXPECT_EQ(100, cd.getWaga());
//    EXPECT_NEAR(1.56, cd.getWzrost(), 0.1);
//    EXPECT_EQ(80, cd.getWiek());
//    EXPECT_EQ(kobieta, cd.getPlec());
//}
//
//TEST(BMItest, calculateBMI)
//{
//	BMI c (80, 1.65, 20, kobieta);
//	EXPECT_NEAR(29.38, c.calculateBMI(), 0.1);
//}
//
//TEST(BMItest, calculateCalories)
//{
//	BMI c (80, 1.65, 20, kobieta);
//	EXPECT_NEAR(1632.11, c.calculateCalories(), 0.1);
//}
//
//
//
//int main(int argc, char **argv) {
//      ::testing::InitGoogleTest(&argc, argv);
//      return RUN_ALL_TESTS();
//}
