/*
 * BMI.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */

#ifndef BMI_HPP_
#define BMI_HPP_

enum plec
{
	kobieta,
	mezczyzna,
	nieokreslona
};

class BMI
{
private:
	float mWaga;
	float mWzrost;
	int mWiek;
	plec mPlec;

public:
	BMI();
	BMI(float waga, float wzrost, int wiek, plec plec);
	~BMI();
	plec getPlec() const;
	void setPlec(plec plec);
	float getWaga() const;
	void setWaga(float waga);
	int getWiek() const;
	void setWiek(int wiek);
	float getWzrost() const;
	void setWzrost(float wzrost);
	float calculateBMI();
	float calculateCalories();
};



#endif /* BMI_HPP_ */
