/*
 * KolorowyPunkt.cpp
 *
 *  Created on: Mar 28, 2017
 *      Author: orlik
 */

#include "KolorowyPunkt.hpp"
#include <iostream>


KolorowyPunkt::KolorowyPunkt(int x, int y, Kolor kolor)
: Punkt(x,y)
, mKolor(kolor)
{
}

void KolorowyPunkt::wypisz()
{
	std::cout<<"["<<mX<<","<<mY<<", kolor= "<<getKolorString()<<"]\n";
}

std::string KolorowyPunkt::getKolorString()
{
	std::string kolorek("");

	switch (mKolor)
	{
	case czarny:
		kolorek = "Czarny";
		break;
	case bialy:
		kolorek = "Bialy";
		break;
	case zielony:
		kolorek = "Zielony";
		break;
	default:
		kolorek = "Dziwny";
		break;
	}

	return kolorek;
}
