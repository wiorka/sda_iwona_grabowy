#include <iostream>
#include <cstdlib>
using namespace std;



void pokazMacierz(int tablica[5][5]) {
    for (int y = 0; y < 5; ++y)
    {
        for (int x = 0; x < 5; ++x)
        {
            if (tablica[y][x] < 10)
            {
                std::cout << ' ';
            }
            std::cout << tablica[y][x] << ' ';
        }
        std::cout << '\n';
    }
    std::cout << "-------------------" << std::endl;
    std::cout << std::flush;
}

int main()
{
int macierz[5][5] = {{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}, {11, 12, 13, 14, 15}, {16, 17, 18, 19, 20}, {21, 22, 23, 24, 25}};


pokazMacierz(macierz);

int suma1= 0;
int suma2= 0;
int suma3= 0;

for (int i=0; i<5; i++)
{
    for (int k=0; k<5; k++)
    {
        if (i==k)
        {
            suma1 = suma1 + macierz [i][k];
        }
        if (i>k)
        {
            suma2 = suma2 + macierz [i][k];
        }
        if (i<k)
        {
            suma3 = suma3 + macierz [i][k];
        }
    }
}

std::cout << "Suma przekatnej " << suma1 << std::endl;
std::cout << "Suma pod przekatna " << suma2 << std::endl;
std::cout << "Suma nad przekatna " << suma3 << std::endl;

return 0;
}
