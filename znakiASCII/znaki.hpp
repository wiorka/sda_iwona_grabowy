/*
 * znaki.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include <string>
#ifndef ZNAKI_HPP_
#define ZNAKI_HPP_

class Znaki
{
private:
	std::string mStr;

public:
	Znaki(std::string str) :
			mStr(str)
	{
	}

	int sumuj()
	{
		int sum = 0;
		for (int i = 0; i < mStr.size(); i++)
		{
			sum += mStr[i];
		}
		return sum;
	}

	const std::string& getStr() const
	{
		return mStr;
	}

	void setStr(const std::string& str)
	{
		mStr = str;
	}
};

#endif /* ZNAKI_HPP_ */
