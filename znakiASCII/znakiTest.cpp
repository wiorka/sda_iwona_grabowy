/*
 * znakiTest.cpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */


#include "gtest/gtest.h"
#include "znaki.hpp"
#include <iostream>


TEST(Znaki, Constructor)
{
	Znaki z("fajny tekst");
	EXPECT_EQ("fajny tekst", z.getStr());
}

TEST(Znaki, sumuj)
{
Znaki z ("Write a program that takes in a string from the user (or if you want, a file!) and prints all the characters' ascii values summed up to the user.");
EXPECT_EQ(13124, z.sumuj());
}


int main(int argc, char **argv) {
      ::testing::InitGoogleTest(&argc, argv);
      return RUN_ALL_TESTS();
}

//int main()
//{
//	Znaki znaki("Write a program that takes in a string from the user (or if you want, a file!) and prints all the characters' ascii values summed up to the user.");
//	std::cout << znaki.sumuj() << std::endl;
//
//
//	return 0;
//}
