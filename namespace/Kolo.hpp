/*
 * Kolo.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "Figura.hpp"
#include "namespace.hpp"
#include <iostream>
#ifndef KOLO_HPP_
#define KOLO_HPP_

class Kolo: public Figura
    {
    int mR;
    Kolor::Kolor mKolor;
public:
    Kolo(int r, Kolor::Kolor kolor) :
	    mR(r), mKolor(kolor)
	{
	}
    ~Kolo()
	{
	}
    void wypisz()
	{
std::cout << "Promien: " << mR << " kolor: " << Kolor::convertToString(mKolor) << std::endl;
	}
    };

#endif /* KOLO_HPP_ */
