/*
 * Kwadrat.hpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include "Figura.hpp"
#include "namespace.hpp"
#include <iostream>
#ifndef KWADRAT_HPP_
#define KWADRAT_HPP_

class Kwadrat: public Figura
    {
    int mA;
    Kolor::Kolor mKolor;
public:
    Kwadrat(int a, Kolor::Kolor kolor) :
	    mA(a), mKolor(kolor)
	{
	}
    ~Kwadrat()
	{
	}
    void wypisz()
	{
	std::cout << "Bok: " << mA << " kolor: " << Kolor::convertToString(mKolor) << std::endl;
	}

    };

#endif /* KWADRAT_HPP_ */
