/*
 * main.cpp
 *
 *  Created on: 26.04.2017
 *      Author: RENT
 */
#include <iostream>
#include "namespace.hpp"
#include "Kwadrat.hpp"
#include "Kolo.hpp"

int main()
    {
std::cout << Kolor::convertToString(Kolor::ciemnaButelkowaZielen) << std::endl;

Kolo kolo(10, Kolor::niebieski);
Kwadrat kwadrat (4, Kolor::pomaranczowy);
kolo.wypisz();
kwadrat.wypisz();

Figura* tab[3];
tab[0] = new Kolo(12, Kolor::pomaranczowy);
tab[1] = new Kolo(11, Kolor::niebieski);
tab[2] = new Kwadrat( 2, Kolor::ciemnaButelkowaZielen);


for (int i = 0; i <3; i++)
{
	   tab[i]->wypisz();
}

for (int i = 0; i <3; i++)
{
	delete tab[i];
}

    return 0;
    }

