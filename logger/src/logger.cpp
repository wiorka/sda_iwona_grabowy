//============================================================================
// Name        : logger.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================
#include "logger.hpp"
#include <iostream>
#include <fstream>


Logger::Logger (std::string filename, int log_level)
:file(filename.c_str(), std::ofstream::out)
,level(log_level)
{
}


Logger::~Logger ()
{
file.close();
}

void Logger::Log(std::string message, int log_level)
{

	if (level < log_level) return;

time_t t;
t = time(NULL);
struct tm* current = localtime(&t);
file << current -> tm_year +1900 << "." << current -> tm_mon +1 << "."
		<< current -> tm_mday << " "<< current -> tm_hour << ":" << current -> tm_min << " "
		<< message << std::endl;

}
