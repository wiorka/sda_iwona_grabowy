#include <iostream>
using namespace std;

class Processor
{
protected:
	int mSerialNr;
public:
	virtual void process()=0;

	Processor(int number)
	:mSerialNr(number)
	{
	}

	virtual ~Processor()
	{
		cout << "~Processor" << endl;
	}
};

class ProcessorAMD: public Processor
{
public:
	void process()
	{
		cout << "AMD nr "<< mSerialNr<< " pracuje" << endl;
	}

	ProcessorAMD(int number)
	:Processor(number)
	{
	}

	~ProcessorAMD()
	{
		cout << "~ProcessorAMD" << endl;
	}

};

class ProcessorIntel: public Processor
{
public:
	void process()
	{
		cout << "Intel nr "<< mSerialNr<< " pracuje" << endl;
	}

	ProcessorIntel(int number)
	:Processor(number)
	{
	}

	~ProcessorIntel()
	{
		cout << "~ProcessorIntel" << endl;
	}

};

class Cooler
{
public:
	virtual void cool()=0;

	virtual ~Cooler()
	{
		cout << "~Cooler" << endl;
	}
};

class CoolerAMD: public Cooler
{
public:
	void cool()
	{
		cout << "AMD ch�odzi" << endl;
	}

	~CoolerAMD()
	{
		cout << "~CoolerAMD" << endl;
	}

};

class CoolerIntel: public Cooler
{
public:
	void cool()
	{
		cout << "Intel ch�odzi" << endl;
	}

	~CoolerIntel()
	{
		cout << "~CoolerIntel" << endl;
	}

};


class AbstractSemiconducorFactory
{
public:
	virtual Processor* createProcessor() =0;
	virtual Cooler* createCooler() =0;

	virtual ~AbstractSemiconducorFactory()
	{
	}
};

class IntelSemiconducorFactory : public AbstractSemiconducorFactory
{
private:
	int mSerialCounter;

public:

	IntelSemiconducorFactory()
	{
		mSerialCounter = 0;
	}

	Processor* createProcessor()
	{
		mSerialCounter++;
		return new ProcessorIntel(mSerialCounter);
	}

	Cooler* createCooler()
	{
		return new CoolerIntel();
	}

};

class AMDSemiconducorFactory : public AbstractSemiconducorFactory
{
	int mSerialCounter;
public:

	AMDSemiconducorFactory()
	{
		mSerialCounter = 0;
	}
	Processor* createProcessor()
	{
		mSerialCounter+=10;
		return new ProcessorAMD(mSerialCounter);
	}

	Cooler* createCooler()
	{
		return new CoolerAMD();
	}

};

class Computer
{
private:
string mName;
Processor* mProcessor;
Cooler* mCooler;

public:

Computer(string name, AbstractSemiconducorFactory& factory)
:mName(name)
{
mProcessor = factory.createProcessor();
mCooler = factory.createCooler();
}

~Computer()
{
	delete mProcessor;
	delete mCooler;
}

void run()
{
	cout<<"Nazywam sie:"<<mName<<endl;
	mProcessor->process();
	mCooler->cool();
}

};


int main()
{
IntelSemiconducorFactory intelFactory;
AMDSemiconducorFactory amdFactory;

Computer intelPC1("PC1", intelFactory);
Computer amdPC1("PC2", amdFactory);
Computer intelPC2("PC3", intelFactory);
Computer amdPC2("PC4", amdFactory);
Computer intelPC3("PC5", intelFactory);
Computer amdPC3("PC6", amdFactory);

intelPC1.run();
amdPC1.run();
intelPC2.run();
amdPC2.run();
intelPC3.run();
amdPC3.run();

	return 0;
}
