/*
 * liquid.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef LIQUID_HPP_
#define LIQUID_HPP_

class Liquid
    {
protected:
    int mAmount;
public:
    virtual void add (int amount) = 0;
    virtual void remove (int amount) = 0;
    virtual void removeAll () = 0;

    Liquid(int amount)
    :mAmount(amount)
	{
	}

    virtual ~ Liquid()
	{
	}

    int getAmount() const
	{
	return mAmount;
	}
    };

#endif /* LIQUID_HPP_ */
