/*
 * cup.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef CUP_HPP_
#define CUP_HPP_
# include "liquid.hpp"
# include "rum.hpp"
# include "coffee.hpp"
# include "espresso.hpp"
# include "decaff.hpp"
# include "milk.hpp"
#include <cstdlib>
#include <iostream>

class Cup
    {
private:
    Liquid ** mContent;
    size_t mSize;


    void resize()
	{
	if (mSize == 0)
	    {
	    mContent = new Liquid *[++mSize];
	    }
	else
	    {
	    Liquid** tmp = new Liquid *[++mSize];
	    for (size_t i = 0; i < mSize; i++)
		{
		tmp[i] = mContent[i];
		}
	    delete[] mContent;
	    mContent = tmp;
	    }
	}

    void clear()
	{
	for (size_t i = 0; i < mSize - 1; i++)
	    {
	    delete mContent[i];
	    }
	delete[] mContent;

	mContent = 0;
	mSize = 0;
	}

    int calculateLiquidAmount()
	{
	int liquidAmount = 0;
	if (mSize == 0)
	    {
	    //Nic
	    }
	else
	    {
	    for (size_t i = 0; i < mSize; i++)
		{
		liquidAmount += mContent[i]->getAmount();
		}
	    }
	return liquidAmount;
	}

public:
    Cup ()
:mContent(0)
,mSize(0)
    {
    }

    void add(Liquid* liquid)
	{
	resize();
	mContent[mSize - 1] = liquid;
	}

    void add(const Milk &liquid)
	{
	resize();
	mContent[mSize - 1] = new Milk(liquid);
	}

    void add(const Coffee &liquid)
	{
	resize();
	mContent[mSize - 1] = new Coffee(liquid);
	}

    void add(const Rum &liquid)
	{
	resize();
	mContent[mSize - 1] = new Rum(liquid);
	}

    void takeSip(int amount)
	{
	int liquidAmount = calculateLiquidAmount();
	if (liquidAmount > 0 && amount > 0)
	    {
	    if (liquidAmount <= amount)
		{
		spill();
		}
	    else
		{
		for (size_t i = 0; i < mSize; i++)
		    {
		    float liquidRatio = (float) mContent[i]->getAmount() / (float) liquidAmount;
		    mContent[i]->remove(liquidRatio * amount);
		    }
		}
	    }
	}

    void spill()
	{
	for (size_t i = 0; i < mSize - 1; i++)
	    {
	    mContent[i]->removeAll();
	    }
	clear();
	}

    void call()
	{
	for (size_t i = 0; i < mSize; i++)
	    {
	    std::cout << mContent[i] -> getAmount() << std::endl;
	    }
	}


    };

#endif /* CUP_HPP_ */
