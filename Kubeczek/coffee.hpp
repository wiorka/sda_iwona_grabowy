/*
 * coffee.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef COFFEE_HPP_
#define COFFEE_HPP_
# include "liquid.hpp"

class Coffee: public Liquid
    {
protected:
    int mCaffeine;
public:
    Coffee(int amount, int caffeine) :
	    Liquid(amount), mCaffeine(caffeine)
	{
	}

    virtual ~ Coffee()
	{
	}

    virtual void add(int amount)
	{
	mAmount += amount;
	}

    virtual void remove(int amount)
	{
	mAmount = (mAmount > amount) ? mAmount-amount : 0;
	}

    virtual void removeAll()
	{
	mAmount = 0;
	}

    };

#endif /* COFFEE_HPP_ */
