/*
 * milk.hpp
 *
 *  Created on: 08.05.2017
 *      Author: RENT
 */

#ifndef MILK_HPP_
#define MILK_HPP_
# include "liquid.hpp"

class Milk: public Liquid
    {
protected:
    float mFat;
public:
    Milk(int amount, float fat) :
	    Liquid(amount), mFat(fat)
	{
	}

    void add(int amount)
	{
	mAmount += amount;
	}

    void remove(int amount)
	{
	mAmount = (mAmount > amount) ? mAmount-amount : 0;
	}

    void removeAll()
	{
	mAmount = 0;
	}
    };

#endif /* MILK_HPP_ */
