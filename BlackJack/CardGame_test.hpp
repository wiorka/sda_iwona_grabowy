/*
 * CardGame_test.hpp
 *
 *  Created on: 25.04.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include"Card.hpp"
#include"Deck.hpp"

TEST(CardClassTest, cardCreation)
    {
    Card card;
    EXPECT_EQ(0, card.getValue());
    EXPECT_EQ(Card::ColourEnd, card.getColour());
    EXPECT_EQ(Card::FigureEnd, card.getFigure());
    }

TEST(CardClassTest, cardSetValue)
    {
    Card card;
    card.setValues(Card::Kier, Card::FIGURE_6);
    EXPECT_EQ(6, card.getValue());
    EXPECT_EQ(Card::Kier, card.getColour());
    EXPECT_EQ(Card::FIGURE_6, card.getFigure());
    }

TEST(DeckClassTest, getNextCard)
    {
    Deck deck;
    Card card;
    card = deck.getNextCard();
    EXPECT_NE(0, card.getValue());
    EXPECT_NE(Card::ColourEnd, card.getColour());
    EXPECT_NE(Card::FigureEnd, card.getFigure());
    }

TEST(DeckClassTest, getNextCard52)
    {
    Deck deck;
    Card card;
for (int i = 0; i <52; i ++)
	{
    card = deck.getNextCard();
	}
        EXPECT_NE(0, card.getValue());
    EXPECT_NE(Card::ColourEnd, card.getColour());
    EXPECT_NE(Card::FigureEnd, card.getFigure());
    }

int main(int argc, char **argv)
    {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    }
