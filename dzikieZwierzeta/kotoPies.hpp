/*
 * kotoPies.hpp
 *
 *  Created on: 03.04.2017
 *      Author: RADEK
 */
#include "dzikieZwierzeta.hpp"
#include "Kot.hpp"
#include "Pies.hpp"
#ifndef KOTOPIES_HPP_
#define KOTOPIES_HPP_

class KotoPies: public Kot, public Pies
{
public:
	KotoPies (std::string imie)
{
		mImie = imie;
}
	void przedstawSie()
	{
		std:: cout << "Czesc, jestem " << mImie << std::endl;
	}

	void dajGlos()
	{
		Kot::dajGlos();
		Pies::dajGlos();
	}
};


#endif /* KOTOPIES_HPP_ */
