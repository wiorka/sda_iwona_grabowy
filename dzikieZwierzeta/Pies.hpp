/*
 * Pies.hpp
 *
 *  Created on: 31.03.2017
 *      Author: RADEK
 */
#include "dzikieZwierzeta.hpp"
#ifndef PIES_HPP_
#define PIES_HPP_

class Pies: public virtual DzikieZwierze
{

public:

	Pies (std::string imie)
:DzikieZwierze(imie)
{
		std:: cout << "Pies (std::string imie) " <<  std::endl;
}
	Pies()
	{
		std:: cout << "Pies() " << std::endl;
	}

	~Pies()
	{
		std:: cout << "~Pies() " << std::endl;
	}

	void przedstawSie()
	{
		std:: cout << "Czesc, jestem " << mImie << std::endl;
	}

	void dajGlos()
	{
		std::cout << "Hau hau" << std::endl;
	}
};




#endif /* PIES_HPP_ */
