/*
 * dzikieZwierzeta.hpp
 *
 *  Created on: 31.03.2017
 *      Author: RADEK
 */

#ifndef DZIKIEZWIERZETA_HPP_
#define DZIKIEZWIERZETA_HPP_
#include <iostream>

class DzikieZwierze
{
protected:
	std::string mImie;

public:
	virtual void dajGlos() = 0;
	virtual void przedstawSie() = 0;

DzikieZwierze()
	{
	std:: cout << "DzikieZwierze()" << std::endl;
	}

	DzikieZwierze(std::string imie)
	:mImie(imie){
	std:: cout << "DzikieZwierze(std::string imie)" << std::endl;
	}

 virtual ~DzikieZwierze()
		{
		std:: cout << "~DzikieZwierze()" << std::endl;
		}
};



#endif /* DZIKIEZWIERZETA_HPP_ */
