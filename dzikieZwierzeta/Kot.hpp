/*
 * Kot.hpp
 *
 *  Created on: 31.03.2017
 *      Author: RADEK
 */
#include "dzikieZwierzeta.hpp"
#ifndef KOT_HPP_
#define KOT_HPP_

class Kot: public virtual DzikieZwierze
{

public:
	Kot (std::string imie)
:DzikieZwierze(imie)
{
		std:: cout << "Kot (std::string imie) " <<  std::endl;
}
	Kot()
	{
		std:: cout << "Kot() " << std::endl;
	}

	~Kot()
	{
		std:: cout << "~Kot() " << std::endl;
	}
	void przedstawSie()
	{
		std:: cout << "Czesc, jestem " << mImie << std::endl;
	}

	void dajGlos()
	{
		std::cout << "Miau miau" << std::endl;
	}
};



#endif /* KOT_HPP_ */
