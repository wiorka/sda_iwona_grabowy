/*
 * main.cpp
 *
 *  Created on: 31.03.2017
 *      Author: RADEK
 */
#include "Kot.hpp"
#include "Pies.hpp"
#include "dzikieZwierzeta.hpp"
#include "kotoPies.hpp"

int main() {

	DzikieZwierze* zwierzak[7];

	zwierzak[0] = new Pies("Kokuz");
	zwierzak[1] = new Kot("Wojtek");
	zwierzak[2] = new Pies("Kika");
	zwierzak[3] = new KotoPies ("Kotopiesek");
	zwierzak[4] = new Pies("Almo");
	zwierzak[5] = new Kot("Wojtek");
	zwierzak[6] = new KotoPies ("Kotkopies");
	zwierzak[7] = new Pies("Frida");

for (int i =0; i <8; i++)
	{
	    zwierzak[i]-> przedstawSie();
		zwierzak[i]-> dajGlos();
	}

	for (int i =0; i <8; i++)
		{
			delete zwierzak[i];
		}

		return 0;
	}
