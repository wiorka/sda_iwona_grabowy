/*
 * haslo.hpp
 *
 *  Created on: 22.04.2017
 *      Author: RENT
 */
#include <string>
#include <cstdlib>
#include <ctime>
#include <iostream>
#ifndef HASLO_HPP_
#define HASLO_HPP_

class Haslo
{
	unsigned int mIloscZnakow;

public:
	Haslo(unsigned int iloscZnakow) :
			mIloscZnakow(iloscZnakow)
	{
	}

	std::string generujHaslo()
	{
		std::string pass;
		pass += wylosujCyfre();
		pass += wylosujMalaLitere();
		pass += wylosujWielkaLitere();

		for (unsigned int i = 3; i < mIloscZnakow; i++)
		{
			int x = rand()%3;
			if (x==0)
			{
				pass += wylosujCyfre();
			}
			else if (x==1)
			{
				pass += wylosujMalaLitere();
			}
			else
			{
				pass += wylosujWielkaLitere();
			}
		}

		przemieszaj(pass);

		return pass;
	}
	char wylosujCyfre()
	{
		int cyfra;
		cyfra = std::rand() % 10;
		return '0' + cyfra;

	}
	char wylosujMalaLitere()
	{
		int mala;
		mala = std::rand() % 26;
		return 'a' + mala;

	}
	char wylosujWielkaLitere()
	{
		char wielka;
		wielka = std::rand() % 26;
		return 'A' + wielka;
	}

	void przemieszaj (std::string& str)
	{
		for (int i = str.length(); i >0; i--)
		{
			int pos = rand()%str.length();
			char tmp = str[i-1];
			str[i-1] = str [pos];
			str[pos] = tmp;
		}
	}
};

#endif /* HASLO_HPP_ */
