/*
 * main.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RADEK
 */
#include <iostream>
#include <string>

#include "pracujacyStudent.hpp"

int main()
{

	PracujacyStudent Mietek(4567,"Kurier", "Logistyka", 3, 85095476598);
	Pracownik pracownik(1111.0, "Tynkarz", 89051569842);
	Student student("Gotowanie na gazie", 2, 90051563254);

	Pracownik *wskPrac;
	Student *wskStud;

	pracownik.pracuj();
	wskPrac = & pracownik;
	wskPrac->pracuj();

	student.uczSie();
	wskStud = & student;
	wskStud->uczSie();

	wskPrac = &Mietek;
	wskStud = &Mietek;
	Mietek.pracuj();
	Mietek.uczSie();
	wskPrac->pracuj();
	wskStud->uczSie();

		return 0;


	return 0;
}

