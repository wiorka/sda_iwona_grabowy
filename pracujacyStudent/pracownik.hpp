/*
 * pracownik.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RADEK
 */

#ifndef PRACOWNIK_HPP_
#define PRACOWNIK_HPP_
#include <iostream>
#include <string>
#include "Osoba.hpp"

class Pracownik: public virtual Osoba
{
	float mPensja;
	std::string mZawod;
public:
	virtual void pracuj();

	Pracownik(float pensja, std::string zawod, long PESEL);
	Pracownik(float pensja, std::string zawod);
	virtual ~Pracownik();

};

#endif /* PRACOWNIK_HPP_ */
