/*
 * pracujacyStudent.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RADEK
 */

#ifndef PRACUJACYSTUDENT_HPP_
#define PRACUJACYSTUDENT_HPP_
#include <iostream>
#include <string>
#include "student.hpp"
#include "pracownik.hpp"

class PracujacyStudent: public Pracownik, public Student
{

public:
	void pracuj();
	void uczSie();

	PracujacyStudent (float pensja, std::string zawod, std::string kierunek, int rokStudiow, long PESEL);
	~PracujacyStudent();

};



#endif /* PRACUJACYSTUDENT_HPP_ */
