/*
 * student.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RADEK
 */
#include "student.hpp"
#include <iostream>
#include <string>


Student::Student (std::string kierunek, int rokStudiow, long PESEL)
:Osoba(PESEL)
,mKierunek(kierunek)
,mRokStudiow(rokStudiow)
{
}

Student::Student (std::string kierunek, int rokStudiow)
:mKierunek(kierunek)
,mRokStudiow(rokStudiow)
{
}

Student::~Student()
{
}

void Student::uczSie()
{
	std::cout << "Ucze sie pilnie!" << std::endl;
}
