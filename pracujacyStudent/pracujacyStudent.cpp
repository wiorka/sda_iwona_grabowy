/*
z * pracujacyStudent.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RADEK
 */
#include "pracujacyStudent.hpp"


 PracujacyStudent::PracujacyStudent (float pensja, std::string zawod, std::string kierunek, int rokStudiow, long PESEL)
 : Osoba(PESEL)
 ,Pracownik (pensja, zawod)
 ,Student (kierunek, rokStudiow)
 {
 }

 PracujacyStudent::~PracujacyStudent()
 {
 }

 void PracujacyStudent::pracuj()
{
	std::cout << "Nie mam czasu na prace!" << std::endl;
}

 void PracujacyStudent::uczSie()
{
	std::cout << "Nie mam czasu na nauke!" << std::endl;
}
