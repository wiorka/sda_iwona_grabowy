/*
 * pracownik.cpp
 *
 *  Created on: 27.03.2017
 *      Author: RADEK
 */
#include "pracownik.hpp"
#include <iostream>
#include <string>

void Pracownik::pracuj()
{
	std::cout << "Pracuje i zarabiam!" << std::endl;
}

Pracownik::Pracownik(float pensja, std::string zawod, long PESEL) :
		Osoba(PESEL), mPensja(pensja), mZawod(zawod)
{
}

Pracownik::Pracownik(float pensja, std::string zawod) :
		mPensja(pensja), mZawod(zawod)
{
}

Pracownik::~Pracownik()
{
}
