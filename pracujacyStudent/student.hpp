/*
 * student.hpp
 *
 *  Created on: 27.03.2017
 *      Author: RADEK
 */

#ifndef STUDENT_HPP_
#define STUDENT_HPP_
#include <iostream>
#include <string>
#include "Osoba.hpp"

class Student: public virtual Osoba
{
	std::string mKierunek;
	int mRokStudiow;
public:
	virtual void uczSie();
	Student (std::string kierunek, int rokStudiow, long PESEL);
	Student (std::string kierunek, int rokStudiow);
	virtual ~Student();
};




#endif /* STUDENT_HPP_ */
