/*
 * main.cpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include "shape.hpp"
#include "circle.hpp"
#include "cylinder.hpp"
#include <iostream>


int main()
{

Circle kolo(5);
kolo.nazwa();
std::cout << kolo.pole() << std::endl;


Cylinder walec (3,4);
walec.nazwa();
std::cout << walec.pole() << std::endl;


return 0;
}
