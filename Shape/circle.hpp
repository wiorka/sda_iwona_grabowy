/*
 * circle.hpp
 *
 *  Created on: 08.04.2017
 *      Author: RENT
 */
#include "shape.hpp"
#include <iostream>
#ifndef CIRCLE_HPP_
#define CIRCLE_HPP_

class Circle: public Shape
{
protected:
	int mR;
	const float mPi = 3.14;

public:


	Circle(int r)
:mR(r)
{
}
	~Circle()
	{
	}

float pole()
{
	return mPi*mR*mR;
}

void nazwa ()
{
	std::cout << "Hej, jestem ko�o" << std::endl;
}
};



#endif /* CIRCLE_HPP_ */
