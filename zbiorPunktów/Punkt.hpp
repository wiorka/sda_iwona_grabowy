/*
 * Punkt.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef PUNKT_HPP_
#define PUNKT_HPP_

class Punkt
    {
private:
    float mX;
    float mY;
public:
    float getX() const
	{
	return mX;
	}

    void setX(float x)
	{
	mX = x;
	}

    float getY() const
	{
	return mY;
	}

    void setY(float y)
	{
	mY = y;
	}

    Punkt(float x, float y) :
	    mX(x), mY(y)
	{
	}

    Punkt() :
	    mX(0), mY(0)
	{
	}

    Punkt(const Punkt& p) :
	    mX(p.getX()), mY(p.getY())
	{
	}

    void wypisz()
	{
	std::cout << "x= " << mX << " y: " << mY << endl;
	}

    Punkt operator+ (const Punkt& p) const
	    {
	    Punkt nowy (0,0);
	    nowy.setX(p.getX() + this->getX());
	    nowy.setY(p.getY() + this->getY());
	    return nowy;
	    }

    Punkt operator+ (const float x) const
	    {
	    Punkt nowy (0,0);
	    nowy.setX(this->getX() + x);
	    nowy.setY(this->getY() + x);
	    return nowy;
	    }

    Punkt operator* (const Punkt& p) const
	    {
	    Punkt nowy (0,0);
	    nowy.setX(p.getX() * this->getX());
	    nowy.setY(p.getY() * this->getY());
	    return nowy;
	    }

    Punkt operator* (const float x) const
	    {
	    Punkt nowy (0,0);
	    nowy.setX(this->getX() * x);
	    nowy.setY(this->getY() * x);
	    return nowy;
	    }

    Punkt& operator++ ()
    	    {
        this -> setX(mX+1);
        this -> setY(mY+1);
        return *this;
    	    }

    Punkt operator++ (int)
	    {
	Punkt tmp (*this);
	this -> operator++();
	return tmp;
	    }
    };

Punkt operator+ (const float x, const Punkt& p)
    {
    return p+x;
    }

Punkt operator* (const float x, const Punkt& p)
    {
    return p*x;
    }



#endif /* PUNKT_HPP_ */
