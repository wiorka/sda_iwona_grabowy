/*
 * Zbior.hpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */

#ifndef ZBIOR_HPP_
#define ZBIOR_HPP_
# include "Punkt.hpp"
#include <iostream>

class Zbior
    {
private:
    Punkt** mP;
    size_t mSize;

    void resize()
	{
	if (mSize == 0)
	    {
	    mP = new Punkt*[++mSize];
	    }
	else
	    {
	    Punkt** tmp = new Punkt*[++mSize];
	    for (size_t i = 0; i < mSize - 1; i++)
		{
		tmp[i] = mP[i];
		}
	    delete[] mP;
	    mP = tmp;
	    }
	}

public:
    Zbior() :
	    mP(0), mSize(0)
	{
	}

    ~Zbior()
	{
	if (mSize != 0)
	    {
	    for (size_t i = 0; i < mSize; i++)
		{
		delete mP[i];
		}
	    }
	delete[] mP;
	mSize = 0;
	}

    void wypisz()
	{
	for (size_t i = 0; i < this->getSize(); i++)
	    {
	    std::cout << "P" << i << " {";
	    mP[i]->wypisz();
	    std::cout << "}" << endl;
	    }
	}

    Punkt operator[](size_t idx) const
	{
	if (idx < mSize)
	    {
	    return Punkt(*mP[idx]);
	    }
	else
	    {
	    return Punkt(0, 0);
	    }
	}
    void operator+(const Punkt& p)
	{
	resize();
	mP[mSize - 1] = new Punkt(p);
	}

    void operator+(const Zbior& zbior)
	{
	for (size_t i = 0; i < zbior.getSize(); i++)
	    {
	    *this + zbior[i];
	    }
	}

    operator bool()
	{
	return (this->getSize() != 0);
	}

    size_t getSize() const
	{
	return mSize;
	}

    void setSize(size_t size)
	{
	mSize = size;
	}
    };

#endif /* ZBIOR_HPP_ */
