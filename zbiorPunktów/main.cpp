/*
 * main.cpp
 *
 *  Created on: 06.05.2017
 *      Author: RENT
 */
#include <iostream>
using namespace std;
#include "Zbior.hpp"

int main()
    {

    Punkt p1(5, 4);
    Punkt p2(3, 5);
    Punkt p3 = ++p1 + p2;
    p3.wypisz();
    Punkt p4 = p2 + 3.2;
    p4.wypisz();
    Punkt p5 = 5.3 + p1;
    p5.wypisz();

    Zbior zbior;
    if (zbior)
	{
	std::cout << "Prawidlowy" << endl;
	}
    else
	{
	std::cout << "Nieprawidlowy" << endl;
	}
    zbior + p1;
    zbior + p2;
    zbior + p3;
    zbior.wypisz();

    Zbior zb;
    zb + Punkt(12, 23);
    zb + Punkt(10, 5);
    zb + Punkt(54, 3);

    zbior + zb;
    zbior.wypisz();
    }

