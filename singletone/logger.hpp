/*
 * logger.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef LOGGER_HPP_
#define LOGGER_HPP_
# include <iostream>
#include <fstream>
using namespace std;

class Logger
    {
private:
    ofstream mFile;
    static Logger* s_instance;
    Logger();
public:
    static Logger* instance();
    void log(string dataToLog);
    };

#endif /* LOGGER_HPP_ */
