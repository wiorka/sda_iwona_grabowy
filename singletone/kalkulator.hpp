/*
 * kalkulator.hpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */

#ifndef KALKULATOR_HPP_
#define KALKULATOR_HPP_
#include "logger.hpp"

class Calculator
    {
public:
    int podziel(int a, int b)
	{
	Logger::instance() ->log("Kalkulator::podziel");
	if (b == 0)
	    {
	    Logger::instance() ->log("Nie dzieli si� przez 0");
	    return 0;
	    }
	else
	    {
	    return a / b;
	    }
	}
    };

#endif /* KALKULATOR_HPP_ */
