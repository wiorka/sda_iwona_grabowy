/*
 * logger.cpp
 *
 *  Created on: 13.05.2017
 *      Author: RENT
 */
#include "logger.hpp"

Logger* Logger::s_instance = 0;

Logger::Logger()
    {
    mFile.open("log.txt");
    }

Logger* Logger::instance()
{
    if (!s_instance)
	{
	s_instance = new Logger;
	}
    return s_instance;
}

void Logger::log(string dataToLog)
    {
    mFile << dataToLog << endl;
    }

